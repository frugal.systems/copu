<div id="aboutOverlay">
        <button id="aboutOverlayClose">X</button>
            <div class="aboutContent">
                Welcome to CoPu! <br>
                <br>
                This is an example of the about overlay with some slider sections. There's not really any info here...<br>
                <br>
                <button class="aboutOverlayNext">next &#8594;</button>
            </div><!--end aboutContent1 -->
            <div class="aboutContent">
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Veritatis repudiandae et quas eaque ex, architecto suscipit, vero molestias quisquam distinctio magni deleniti laboriosam corporis labore praesentium, ad aut consectetur tenetur!
                <br>
                <div class="buttonContainer">
                    <button class="aboutOverlayPrev">&#8592; previous</button>
                    <button class="aboutOverlayNext">next &#8594;</button>
                </div>
            </div>
            <div class="aboutContent">
            Lorem ipsum dolor sit, amet consectetur adipisicing elit. Dolorem vitae repellat molestiae odit perferendis exercitationem corporis dignissimos possimus aliquid animi recusandae expedita accusamus at minus, omnis, laboriosam fugiat aperiam modi.
            <div class="buttonContainer">
                <button class="aboutOverlayPrev">&#8592; previous</button>
                <button class="aboutOverlayNext">next &#8594;</button>
            </div>

            </div><!--aboutContent2 -->
            <div class=aboutContent>
                Lorem ipsum dolor sit amet consectetur adipisicing elit. Eligendi, perspiciatis incidunt adipisci quis saepe velit provident aut reiciendis harum reprehenderit officia commodi voluptatum quisquam nisi dolore tenetur? Iusto, voluptate officia! <br>
                <br>
                <br>
                <button class=aboutOverlayPrev>&#8592; previous</button>
            </div><!--aboutContent3 -->
</div><!---end aboutOverlay -->