<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <link href="https://fonts.googleapis.com/css2?family=Alegreya:ital,wght@0,400;0,700;1,400&family=Crimson+Text:ital,wght@0,400;0,700;1,400&family=EB+Garamond:ital,wght@0,400;0,700;1,400&family=Josefin+Sans:ital,wght@0,300;0,600;1,300&family=Source+Sans+Pro:ital,wght@0,400;0,700;1,400&display=swap" rel="stylesheet">  

    <link rel="stylesheet" href="css/reset.css" />
    <link rel="stylesheet" href="css/style.css" />
    <link rel="stylesheet" href="css/medium.css" />
    <link rel="stylesheet" href="css/small.css" />
    <title>CoPu | Co-publishing free software template for long-form texts</title>
</head>

<body>
    <?php include 'about.php';?>
    <?php include 'nav.php';?>

    <section class="hiddenBookIndex" id="bookIndex">
        <?php include 'map.php';?>
        <?php include 'book/toc.php';?>
    </section>
    <!--end bookIndex-->

    <section id=footNotesAndAudiobook>
        <div id="audiobookContainer">
            Listen to <span id="bookSection"></span><br>
            <audio id="audioBookFile" controls></audio>
        </div>
        <?php include 'book/footnotes.php';?>
    </section><!--end citationsAndAudiobook-->

    <section id="bookContent">
        <section id="mainBookText">
            <?php include 'book/book.php';?>
        </section> <!--end mainBookText-->
        <section id=glossary>
        <div class="sticky">
            <h5>Glossary</h5>
        </div>
            <div id="bookGlossary"></div>
        </section>
    </section> <!--end bookContent-->

    <footer>
        <nav id="bookTools">
            <button id="buttonIndex" data-tooltip="Table of contents">☰</button>
            <button id="buttonFootNotes" data-tooltip="Footnotes">[ ]</button>
            <button id="buttonGlossary" data-tooltip="Glossary terms">i</button>
            <button id="bookColorButton" data-tooltip="Change color scheme">●<sub id="bookColorIndicator">5</sub></button>
            <button id="bookFontButton" data-tooltip="Change font scheme">T<sub id="bookFontIndicator">4</sub></button>
            <button id="bookBiggerFontButton" data-tooltip="Bigger font">+</button>
            <button id="bookSmallerFontButton" data-tooltip="Smaller font">-</button>
        </nav>

        <nav id="showTools">
            <button class="mainNavButton" id="showToolButton">⦿</button>
        </nav>
    </footer>

</body>
</html>
<script type="text/javascript" src="nav.js"></script>
<script type="text/javascript" src="script.js"></script>