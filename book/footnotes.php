        <blockquote class="footNoteEx" id="fn1">
            <sup>1</sup>What's a footnote? It's a note placed at the bottom of a page of a book or manuscript that comments on or cites a reference for a designated part of the text. Definition from Wordnik. They are handy when they are mentioned, but should otherwise stay out of the way!
        </blockquote>
