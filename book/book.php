<article id="cover">

    <h1>CoPu<br>📖</h1>
    <br>
    <br>
    <h2>A simple free software template for publishing long-form texts</h2>
    <br>
    <br>
    <h4><a href="https://gitlab.com/frugal.systems/copu/-/archive/main/copu-main.zip">Download CoPu <br> (this website)</a></h4>
    <br>
    <br>
    <br>
    <br>

</article>

<article id="CoPu" class="chapter">
    <div id="u1" class="unit">
        <h2 id="Introduction">Introduction</h2>
    </div>
    <div id="u2" class="unit">
        <p>CoPu offers a simple digital publishing format for open-access long-form texts with an optional, experimental co-publishing experience enhanced by Web3 integration. The intention is to make digital texts as accessible as possible and to subsequently turn them into discourses, co-published by the communities that read them. This site is a CoPu instance!</p>
    </div>
</article>

<article class="chapter">
    <div id="u3" class="unit">
        <h2 id="Features">Features of CoPu 0.1</h2>
    </div>
    <div id="u4" class="unit">
    <p>
<em>A frugal vanilla HTML, CSS, and JS codebase.</em><br>
<br>
<em>A Pandoc script</em> that can parse a Markdown file into an HTML template, which is (almost) ready for publishing with CoPu.<br>
<br>
<em>Easily customizable color themes, font pairings and font-sizing.</em><br>
<br>
<em>Dynamic visibility of internal footnotes</em><br>
 When a referenced footnote<sup class="footNote" id="fn1">1</sup> is within view in the main text, it immediately becomes visible in the sidebar.  <br>
<br>

<em>Customizable Menu</em><br>
 Two level side menu for all of your hyperlink needs.<br>
<br>

<em>About Overlay</em><br>
 The menu includes an about that displays as an overlay on top of the rest of the site.<br>
<br>

<em>Audiobook integration</em><br>
Alternate between reading and listening. Each chapter can be individually made available as audio and is loaded automatically. (not available in this example site!)<br>
<br>
<em>Bespoke anchor tags for citing and referencing via URLs </em> <br>
Link to specific chapters and subchapters: <a href="#Introduction">Jump to the introduction</a>.<br>
Link to specific units: <a href="#u12">jump to a specific unit describing the Peer Production License</a>.<br>
These links will guide visitors to the precise location. <br>
<br>

<em>Glossary integration</em> <br>
Create a simple JSON file with glossary terms and their corresponding definitions, the code will scan through your text and turn the terms into interactive elements showing the corresponding definition. <br>
<br>
The code will additionally create a Glossary section at the end with all your terms in alphabetical order. <br>
<br>

<em>Web3 integration or ‘Economic media mode’</em> <br>
An alternative version of the template with Web3 integration based on each paragraph being turned into units that can be individually minted as NFTs and collected by an audience of readers, unveiling a world of features like discourse DAOs, collective fund management,  gated communities, etc.
This feature is not part of the template and requires plenty of manual set-up! You would need to visit the repo of the site of Protocols for Postcapitalist Expression.
</p>
</div>
</article>

<article class="chapter">
    <div id="u5" class="unit">
        <h2 id="About">About</h2>
    </div>
    <div id="u6" class="unit">
        <p>CoPu was developed by <a href="https://pablo.sx">Pablo Somonte Ruano</a> for the publishing of Protocols for Postcapitalist Expression. A book by the <a target="_blank" href="https://economicspace.agency/">Economic Space Agency</a>. The development of CoPu has been partially supported by ECSA and by the co-publishers of the Protocols book, who made a small fund available for further development. Thank you!</p>
    </div>
</article>

<article class="chapter">
    <div id="u7" class="unit">
        <h2 id="Contribute">Contribute!</h2>
    </div>
    <div id="u8" class="unit">
    <p>
    This is the very first version of CoPu, and there’s quite a few things to be done to bring CoPu to a stable state. Thanks to the publishing of Protocols, a small fund can financially support the following improvements: 
    </p>
    <p>
    <ul>
        <li>
            <b>Pandoc parser:</b> The current pandoc template, developed by Daniel Shinbaum, still requires some manual work to fit CoPu’s html structure, both for the main text and for the table of contents. I'm thinking it might be easier to restructure CoPu's html structure to be more similar to some default pandoc script? I'm not sure. Help!
        </li>
        <li>
            <b>Glossary integration:</b> The glossary integration has performance issues on long books with large glossaries, to the point of being unusable in iOS devices. Due to high RAM usage, probably.
        </li>
        <li>
            <b>Web3 templating:</b> The release of Protocols includes some nifty web3 features developed by Aleksa Stojanović for a co-publishing experience. Yet, these remain out of reach as a ‘template’. Help us generalize these features to include them in the template so others can use these features too!
        </li>
        <li>
            <b>Modularize features:</b> How to make it easy for people to include or leave out features they don’t need? Would be great so people only use what they need.
        </li>
        <li>
            Want to contribute and get rewarded? Do you have any other ideas? please contact me at x {at} pablo.sx.  
        </li>
    </ul>
    </p>
    </div>
</article>

<article id="Usage" class="chapter">
<div id="u9" class="unit">
        <h2 id="Template">Template</h2>
    </div>
    <div id="u10" class="unit">
        <p>CoPu's template should be easy to understand to anyone with basic knowledge of html. Users should mostly just mess with the <em>nav.php</em> file to edit the side navigation, <em>about.php</em> for the about overlay window and the files in the 'book' folder, which contains individual php files for the book's content: <em>book.php</em> for the main text, <em>toc.php</em> for the table of contents (toc) and <em>footnotes.php</em> for the footnotes. (PHP is used only to modularize the code, no other php feature is utilized other than the 'include' function).</p>
    </div>
    <div id="u10" class="unit">
        <h2 id="License">License</h2>
    </div>
    <div id="u11" class="unit">
        <p>CoPu emerged from the idea of creating the ultimate open-access experience for a postcapitalist book. We would want to keep CoPu as radical as possible in its postcapitalist origins. Please avoid hosting forks of CoPu on centralized, non-open source infrastructure like Microsoft’s Github.</p>
        <div id="u12" class="unit">
        <p>CoPu is available as free software under a <a href="https://wiki.p2pfoundation.net/Peer_Production_License" target="_blank">Peer Production Licence</a>. Meaning that only other commoners, cooperatives and nonprofits can share and re-use the software, but not commercial entities intent on making profit through the commons without explicit reciprocity. This fork on the original text of the Creative Commons non-commercial variant makes the PPL an explicitly anti-capitalist version of the CC-NC. It only allows commercial exploitation by collectives in which the ownership of the means of production is in the hands of the value creators, and where any surplus is distributed equally among them (and not only into the hands of owners, shareholders or absentee speculators).
        </p>
    </div>
    </div>
</article>